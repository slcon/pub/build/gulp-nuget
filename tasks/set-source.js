/*****************************************************************************************
* Task: set-source */
/**
* Reads `registry` and `source` from `package.json#publishConfig` for the registry 
* endpoint URL and source name, respectively, and sets the source in the global NuGet 
* config file.
*
* Takes input of the user or deploy token credentials from the user. If the user cancels
* (Ctrl-C) or input values are empty, no action is taken.
*
* @task  {set-source}
* @group {Supporting Tasks}
* @order {200}
* 
* npm Config Values
*   publishConfig:registry - Project-Level Endpoint URL 
*
* These values may also be manually maintained in `nuget.config`.
*   
* see: https://docs.gitlab.com/ee/user/packages/nuget_repository/#use-the-gitlab-endpoint-for-nuget-packages
*****************************************************************************************/
(function() { 'use strict';

const colors   = require('ansi-colors');
const Q        = require('q');
const readline = require('readline');
const common   = require('@slcpub/gulp-common');
const nuget    = require('../index');

module.exports = (() =>
  {
  var pkgInfo     = common.getPackageInfo();
  var config      = pkgInfo.publishConfig||{};
  var source      = config.source;
  var endpointURL = config.registry;

  if (!source)
    throw "'publishConfig.source' not found in 'package.json'.";

  if (!endpointURL)
    throw "'publishConfig.registry' not found in 'package.json'.";

  /*-------------------------------------------------------*/
  /* Get the access token from the user. Cancel on Ctrl-C. */
  /*-------------------------------------------------------*/
  console.log(colors.yellow('\nEnter User or Deploy Token credentials (Ctrl-C to cancel).\n'));

  var rl = readline.createInterface({input:process.stdin, output:process.stdout});
  
  var r = Q.promise((resolve, reject) =>
    {
    var cancel = () => reject(common.fatal('Canceled'));

    rl.on      ('SIGINT', () => cancel(console.log()));
    rl.question("Username: ", (username) => 
    rl.question("Password: ", (password) => 
      {
      if (username && password)
        resolve({ username:username, password:password });
      else
        cancel();
      }));
    })
  .finally(() => rl.close());

  /*-------------------------------------------*/
  /* Set the source in the user's config file. */
  /*-------------------------------------------*/
  return r.then(creds => nuget.setNugetSource(source, endpointURL, creds));
  })
})();
