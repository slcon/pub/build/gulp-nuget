/*****************************************************************************************
* update-version */
/**
* Updates Version number include file with `package.json.version`. Must be run before 
* commit.
*
* @task  {update-version}
* @group {Supporting Tasks}
* @order {200}
*****************************************************************************************/
(function() { 'use strict';

const fs       = require('fs');
const Q        = require('q');
const common   = require('@slcpub/gulp-common');
const nuget    = require('../index');

module.exports = (() =>
  {
  const pkgInfo     = common.getPackageInfo();
  const vfname      = `${pkgInfo.name}/${nuget.options.versionFile}`;
  const targetsFile = fs.readFileSync(vfname).toString('utf-8');
  const re          = new RegExp(/<Version>\s*(\d(?:\.\d){2}(?:[-+].+?)?)\s*<\/Version>/);
  const match       = targetsFile.match(re);

  if (!match)
    throw common.fatal("Unable to parse 'Version.targets'");

  if (pkgInfo.version !== match[1])
    {
    var newTargets = targetsFile.replace(re, `<Version>${pkgInfo.version}</Version>`);
    fs.writeFileSync(vfname, Buffer.from(newTargets, "utf-8"));
    }

  return Q.resolve();
  })
})();
