/*****************************************************************************************
* publishPackage */
/**
* Publishes package to NuGet registry. 
* 
* @param   {string}  source  NuGet source name.
* @returns {Promise} Resolved with command output, rejected with fatal error info. 
*****************************************************************************************/
(function() { 'use strict';

const fs     = require('fs');
const common = require('@slcpub/gulp-common');

module.exports = (nuget) => ((source) =>
  {
  var pkgInfo = common.getPackageInfo();
  var pkgName = `${nuget.options.outputDir}/${pkgInfo.name}.${pkgInfo.version}.nupkg`;

  if (!fs.existsSync(pkgName))
    throw common.fatal(`NuGet package "${pkgName}" not found.`);

  return common.run(`dotnet nuget push ${pkgName} -s ${source}`);
  })
})();

