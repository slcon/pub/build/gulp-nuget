/*****************************************************************************************
* setNugetSource */
/**
* Sets a Nuget source in the user's config file (%APPDATA%/NuGet/NuGet.config). Gets the 
* list of sources and removes source for `name` before replacing it.
* 
* @param  {string}  name   Source name.
* @param  {string}  path   Source path/URL.
* @param  {object}  creds  Token credentials, null=none.
* @return {Promise} Resolved if successful, rejected with fatal error.
*****************************************************************************************/
(function() { 'use strict';

const Q      = require('q');
const common = require('@slcpub/gulp-common');

module.exports = (nuget) => ((name, path, creds) =>
  {
  var parms = (creds ? `--username "${creds.username}" --password "${creds.password}" --store-password-in-clear-text` : '');

  return common.run(`dotnet nuget list source`, { quiet:true })
           .then(list   => Q.resolve(new RegExp(name+' \\[(Enabled|Disabled)\\]$', 'm').test(list)))
           .then(exists => exists ? common.run(`dotnet nuget remove source "${name}"`) : Q.resolve())
           .then(()     => common.run(`dotnet nuget add source ${path} --name "${name}" ${parms}`));
  })
})();
