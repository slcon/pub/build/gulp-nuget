/*******************************************************************************
* $Id$
*******************************************************************************/
const gulp   = require('gulp');
const common = require('@slcpub/gulp-common').useGulp(gulp);
const npm    = require('@slcpub/gulp-npm').useGulp(gulp);

common.addTasks();
npm   .addTasks('set-registry');

gulp.task('default', () => common.showUsage());

