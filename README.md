<!--****************************************************************************
* $Id$
*****************************************************************************-->
# @slcpub/gulp-nuget

Provides a base framework of common functions and predefined tasks for use in 
[`gulpfile.js`][Gulp] files that build [NuGet] packages using [npm] for version 
management.

<!-- toc -->

- [Installation](#installation)
- [Usage](#usage)
  * [Task Usage Help](#task-usage-help)
  * [Available Tasks](#available-tasks)
    + [build](#build)
    + [clean](#clean)
    + [pack](#pack)
    + [publish](#publish)
    + [test](#test)
    + [build-lib](#build-lib)
    + [clean-build](#clean-build)
    + [clean-local](#clean-local)
    + [create-local](#create-local)
    + [publish-local](#publish-local)
    + [restore-env](#restore-env)
    + [set-source](#set-source)
    + [sweep-build](#sweep-build)
    + [update-version](#update-version)
- [API](#api)
  * [Options](#options)
  * [Functions](#functions)
    + [addTasks](#addtasks)
    + [msbuild](#msbuild)
    + [publishPackage](#publishpackage)
    + [setNugetSource](#setnugetsource)
- [Building This Package](#building-this-package)
  * [Build Commands](#build-commands)
  * [TL;DR](#tldr)
  * [See Also](#see-also)

<!-- tocstop -->

## Installation

  1) Set the endpoint for the [package registry] using [npm].
  1) Install this package as a development dependency:

```bash
npm install --save-dev @slcpub/gulp-nuget
```
[package registry]: https://gitlab.com/slcon/pub/registry#npm

## Usage

To use this package in the project build process, add it to `gulpfile.js` and 
load any desired tasks:

```js
const gulp   = require('gulp');
const common = require('@slcpub/gulp-nuget').useGulp(gulp);
common({options});
common.addTasks(task...);
```
  - `common({options})` sets any desired [options].
  - `common.addTasks(task...)` adds some or all of the [available tasks] to 
    `gulp` using `gulp.task()`.

### Task Usage Help

See [Task Usage Help in `gulp-common`][usage-help] for information on creating and displaying
usage help content for tasks.

[usage-help]: https://gitlab.com/slcon/pub/build/gulp-common#task-usage-help


### Available Tasks

These tasks may be added to the Gulp instance by calling [addTasks].

#### build

Runs these tasks to clean and build the library for release prior to commit:
  - [clean-build](#clean-build)
  - [update-version](#update-version)
  - [build-lib](#build-lib)

#### clean

Runs these tasks to remove all generated files:
  - [clean-build](#clean-build)
  - [sweep-build](#sweep-build)

#### pack

Creates the ".nupkg" file after [build](#build).

#### publish

Publishes the ".nupkg" file to package registry after [pack](#pack).

#### test

Runs any tests defined in the associated test project, if any, before [build](#build)

#### build-lib

Builds both Debug and Release versions of the library.

#### clean-build

Removes library `bin` and `obj` folders.

**Note:** This sometimes needs to be run twice to fully remove the folders.

#### clean-local

Removes the Local NuGet registry located in `options.localRegistry`.

#### create-local

Creates the Local NuGet registry located in `options.localRegistry` and
adds it as a global NuGet source named for `options.localSource`.

#### publish-local

Publishes the NuGet package to the local NuGet registry.

#### restore-env

Runs `dotnet restore` and `dotnet tool restore` to restore any missing 
.NET packages to the build environment.

#### set-source

Reads `registry` and `source` from `package.json#publishConfig` for the registry endpoint
URL and source name, respectively, and sets the source in the global NuGet config file.

Takes input of the user or deploy token credentials from the user. If the user cancels
(Ctrl-C) or input values are empty, no action is taken.

#### sweep-build

Removes temporary output files folder.

#### update-version

Updates Version number include file with `package.json.version`. Must be run before 
commit.

## API

### Options

Module options may be set with the package [module function] or by setting the
options directly:

```js
const nuget = require('@slcpub/gulp-nuget');
nuget({options});
nuget.options = {options};
```

  - **dotnetPath** `{string}`  
    `dotnet` executable path. Default is `C:/Program Files/dotnet`.
  - **localRegistry** `{string}`  
    Local registry directory. Default is `%USERPROFILE%\NuGetRegistry`.
  - **localSource** `{string}`  
    Local registry source name. Default is `Local`.
  - **outputDir** `{string}`  
    Output folder for temporary files. Default is `output`.
  - **versionFile** `{string}`  
    Name of the `.targets` file that holds the library version.
    Default is `version.targets`.

### Functions

#### addTasks 

Adds [available tasks](#available-tasks) to the [Gulp instance](#useGulp).

  - ***parameters***
    - `{string[]|string...}`
      Name of the task to add. All tasks are added if no names are supplied.

#### msbuild

Runs `dotnet` msbuild tasks.

  - ***parameters***
    - `{string}` Task command.
    - `{string}` Build configuration.
    - `{string}` Additional command-line parameters.
  - ***returns***
    - `{Promise}` Resolved with command output, rejected with fatal error.

#### publishPackage

Publishes package to NuGet registry.

  - ***parameters***
    - `{string}` Source name.
  - ***returns***
    - `{Promise}` Resolved with command output, rejected with fatal error.

#### setNugetSource

Sets a Nuget source in the user's config file `%APPDATA%/NuGet/NuGet.config`. 
Gets the list of sources and removes source for `name` before replacing it.

  - ***parameters***
    - `{string}` Source name.
    - `{string}` Source path/URL.
    - `{string}` Token credentials
      - `{string}`**`username`** Token username.
      - `{string}`**`password`** Token password.
  - ***returns***
    - `{Promise}` Resolved with command output, rejected with fatal error.


## Building This Package

### Build Commands

**`gulp set-registry`** takes input of a [personal access token] or [deploy token] and configures npm for 
[publishing to the package registry][publishing].

[**`npm version`**][npm version] updates **`package.json#version`**, runs **`gulp build`**, commits to
the local repo, and creates a tag for the version.

**`gulp undo-version`** may be run immediately after [npm version]. It reverts the changes and restores
the repository to it's prior state.

[**`npm publish`**][npm publish] is run after **`npm version`** to publish the package to npm registry as defined in 
**`package.json#publishConfig`**.

### TL;DR

After testing is complete and the package is ready to publish:

1) Run [**`npm version`**][npm version]
   * *preversion*: Runs **`npm test`** to run the library tests,
   * Updates the version number in `package.json`,
   * Commits changes and creates a version tag.

1) If, on second thought, the package is not ready,
   * Run **`gulp undo-version`** to remove the tag and restore the repository.

1) If ready to publish,
   * Run **`gulp push`** to update the remote repo with the new version.

1) Run **`gulp set-registry`** to configure npm for publishing to the package registry. This will require
   a [personal access token] or [deploy token].
   
1) Run [**`npm publish`**][npm publish] to publish the package to npm.
    

### See Also

  - [Building a NuGet Package Using npm][build-nuget-package]


[gulp]:                   https://gulpjs.com/docs/en/getting-started/javascript-and-gulpfiles
[npm]:                    https://docs.npmjs.com/about-npm
[NuGet]:                  https://docs.microsoft.com/en-us/nuget/what-is-nuget
[package registry]:       https://gitlab.com/slcon/graystep/registry
[npm publish]:            https://docs.npmjs.com/cli/v8/commands/npm-publish
[npm version]:            https://docs.npmjs.com/cli/v6/commands/npm-version
[build-nuget-package]:    https://gitlab.com/slcon/pub/doc/-/blob/trunk/BuildNuGetPackageUsingNPM.md