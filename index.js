/*****************************************************************************************
* gulp-nuget
*****************************************************************************************/
(function() { 'use strict';

const Q      = require('q');
const common = require('@slcpub/gulp-common');
const $self  = module.exports = common.makeModule(__dirname) 

/*---------*/
/* Options */
/*---------*/
$self.options = Object.defineProperties(
  {
  defaults: 
    {
    dotnetPath:    'C:/Program Files/dotnet',
    localRegistry: `${process.env['USERPROFILE']}\\NugetRegistry`,
    localSource:   'Local',
    outputDir:     './output',
    versionFile:   'Version.targets'
    },
  
  /*---------------------------------------------*/
  /* options.reset() - Reset to default options. */
  /*---------------------------------------------*/
  reset: function() { return Object.assign(this, this.defaults); }
  },
  /*-----------------------------------------------------*/
  /* Path properties automatically update PATH variable. */
  /*-----------------------------------------------------*/
  common.makePathProperty('dotnetPath')
  ).reset();

/*-----------*/
/* Functions */
/*-----------*/
Object.assign($self,
  {
  /*****************************************************************************
  * msbuild */
  /**
  * Runs dotnet msbuild tasks.
  * 
  * @param  {string}  task    Task command
  * @param  {string}  config  Build configuration.
  * @param  {string}  parms   Additional parameters.
  * @return {Promise} Resolved with command output, rejected with fatal error.
  *****************************************************************************/
  msbuild: (task, config, parms) => 
             common.run(`dotnet ${task} -c ${config} -v:quiet --nologo /clp:ErrorsOnly /clp:NoSummary ${parms||''}`)
  });

/*-------*/
/* Tasks */
/*-------*/
Object.assign($self.tasks, 
  {
  /**
  * Builds the library for release prior to commit.
  *
  * @task  {build}
  * @group {Build Tasks}
  * @order {100}
  */
  'build': () => common.runTasks('clean-build', 'update-version', 'build-lib'),

  /**
  * Removes all generated files.
  *
  * @task  {clean}
  * @group {Build Tasks}
  * @order {100}
  */
  'clean': () => common.runTasks('clean-build', 'sweep-build'),

  /**
  * Creates the ".nupkg" file after "build".
  *
  * @task  {pack}
  * @group {Build Tasks}
  * @order {100}
  */
  'pack': () => common.mkdir($self.options.outputDir)
            .then(() => $self.msbuild('pack', 'Release', `--no-build -o ${$self.options.outputDir}`)),

  /**
  * Publishes the ".nupkg" file to package registry after "pack".
  *
  * @task  {publish}
  * @group {Build Tasks}
  * @order {100}
  */
  'publish': () => $self.publishPackage(common.getPackageInfo().publishConfig.source),

  /**
  * Run unit tests before "build".
  *
  * @task  {test}
  * @group {Build Tasks}
  * @order {100}
  */
  'test': () => $self.msbuild('test', 'Release'),

  /**
  * Builds both Debug and Release versions of the library.
  *
  * @task  {build-lib}
  * @group {Supporting Tasks}
  * @order {200}
  */
  'build-lib': () => $self.msbuild('build', 'Debug').then(() => $self.msbuild('build', 'Release')),

  /**
  * Removes library `bin` and `obj` folders.
  * 
  * Note: This sometimes needs to be run twice to fully remove the folders.
  *
  * @task  {clean-build}
  * @group {Supporting Tasks}
  * @order {200}
  */
  'clean-build': () => Q.resolve(common.getPackageInfo().name).then(name => Q.all([common.rmdir(`${name}/bin`), common.rmdir(`${name}/obj`)])),

  /**
  * Removes the Local NuGet registry located in `options.localRegistry`.
  *
  * @task  {clean-local}
  * @group {Supporting Tasks}
  * @order {200}
  */
  'clean-local': () => common.rmdir($self.options.localRegistry),

  /**
  * Creates the Local NuGet registry located in `options.localRegistry` and
  * adds it as a global NuGet source named for `options.localSource`.
  *
  * @task  {create-local}
  * @group {Supporting Tasks}
  * @order {200}
  */
  'create-local': () => common.mkdir($self.options.localRegistry)
                    .then(() => $self.setNugetSource($self.options.localSource, $self.options.localRegistry)),

  /**
  * Publishes the NuGet package to the local NuGet registry.
  *
  * @task  {publish-local}
  * @group {Supporting Tasks}
  * @order {200}
  */
  'publish-local': () => $self.publishPackage($self.options.localSource),

  /**
  * Restores any missing .NET packages to the build environment.
  *
  * @task  {restore-env}
  * @group {Supporting Tasks}
  * @order {200}
  */
  'restore-env': () => Q.all([common.run('dotnet restore -v:quiet'), common.run('dotnet tool restore')]),

  /**
  * Removes output files folder.
  *
  * @task  {sweep-build}
  * @group {Supporting Tasks}
  * @order {200}
  */
  'sweep-build': () => common.rmdir($self.options.outputDir)  
  });
})();
